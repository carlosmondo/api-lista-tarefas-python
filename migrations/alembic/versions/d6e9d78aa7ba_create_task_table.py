"""create task table

Revision ID: d6e9d78aa7ba
Revises:
Create Date: 2019-05-29 13:44:57.261466

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d6e9d78aa7ba"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "task",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("task", sa.String(128), nullable=False),
        sa.Column("done", sa.Boolean, default=False),
    )


def downgrade():
    op.drop_table("task")
