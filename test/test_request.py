from unittest import TestCase
from unittest.mock import Mock

# import pytest

import requests


class TestReq(TestCase):
    def setUp(self):
        self.client = Mock(spec=requests)
        self.return_json = [
            {"id": 1, "task": "foo", "done": True},
            {"id": 2, "task": "bar", "done": False},
        ]

    def test_retorna_todas_tarefas_se_houver_conteudo(self):
        return_json = [
            {"id": 1, "task": "foo", "done": True},
            {"id": 2, "task": "bar", "done": False},
        ]
        mock_response = Mock(status_code=200)
        mock_response.json.return_value = return_json
        self.client.get.return_value = mock_response

        response = self.client.get("api/tasks")
        self.assertEqual(200, response.status_code)
        conteudo = response.json()
        self.assertEqual(return_json, conteudo)

    def test_retornar_status_code_204_se_nao_houver_conteudo(self):
        return_json = []
        mock_response = Mock(status_code=204)
        mock_response.json.return_value = return_json
        self.client.get.return_value = mock_response

        response = self.client.get("api/tasks")
        self.assertEqual(204, response.status_code)
        conteudo = response.json()
        self.assertEqual([], conteudo)

    def test_retorna_uma_tarefa(self):
        return_json = [
            {"id": 1, "task": "foo", "done": True},
            {"id": 2, "task": "bar", "done": False},
        ]
        mock_response = Mock(status_code=200)
        mock_response.json.return_value = return_json[0]
        self.client.get.return_value = mock_response

        response = self.client.get("api/tasks/1")
        self.assertEqual(200, response.status_code)
        conteudo = response.json()
        self.assertEqual({"id": 1, "task": "foo", "done": True}, conteudo)

    """
    def test_cria_tarefa_(self):
        return_json = [{"id": 1, "task": "foo", "done": True}]
        mock_response = Mock(status_code=200)
        mock_response.json.return_value = return_json
        self.client.get.return_value = mock_response

        response = self.client.get("api/tasks")
        conteudo = response.json()
        self.assertEqual({"id": 1, "task": "foo", "done": True}, conteudo[0])
        post_json = {"id": 2, "task": "bar", "done": False}
        mock_response = Mock()
    """
