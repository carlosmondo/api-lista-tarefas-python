# API para Lista de Tarefas

| **`Documentação`**                                                                                                              |
| ------------------------------------------------------------------------------------------------------------------------------- |
| [![Documentação](https://img.shields.io/badge/api-reference-blue.svg)](https://documenter.getpostman.com/view/7337682/S1EWNuhS) |

### Desafio em Python

Objetivo deste desafio é desenvolver um API de um aplicativo de lista de tarefas, capaz de executar operações CRUD.

#### Para atender aos requisitos, foi utilizado:

-   A linguagem de programação [Python](https://docs.python.org/3/)
-   Micro-framework [Flask](http://flask.pocoo.org/) e suas extensões [SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/),
    [Marshmallow](https://flask-marshmallow.readthedocs.io/en/latest/)
-   Banco de dados [SQLite](https://sqlite.org/docs.html)
-   [Postman](https://learning.getpostman.com/docs/postman/launching_postman/installation_and_updates/) para realizar as requições HTTP e
    gerar a documentação para a API

## Rodar

Para rodar o projeto, é necessário ter instalado em sua máquina o Python na sua versão 3.x.x, para checar se você possui o Python e qual a sua versão, utiliza o comando no:

-   cmd

```
py -0
```

-   bash

```shell
$ python3 --version
```

##

Agora é necessario instalar e criar um ambiente virtual, para que a sua aplicação rode de forma isolada:

-   cmd

```
mkdir api_lista_tarefas
cd api_lista_tarefas
py -3 -m venv venv
venv\Scripts\activate
```

-   bash

```shell
$ mkdir api_lista_tarefas
$ cd api_lista_tarefas
$ python3 -m venv venv
$ . venv/bin/activate
```

##

Agora que temos o ambiente pronto, devemos instalar o micro-framework Flask e suas extensões SQLAlchemy e Marshmallow:

-   cmd

```cmd
pip install flask flask-sqlalchemy marshmallow
```

-   bash

```shell
$ pipenv install flask flask-sqlalchemy marshmallow
```

##

Devemos agora utilziar o cógido para a API. Caso na sua pasta api_lista_tarefas, o Flask tenha criado um arquivo app.py, renomeie para "desafio_itflex.py", abra ele e cole este código (caso este arquivo não exista, crie ele):

```python
"""
    Importa os modulos necessarios
    - Flask para criar a instancia da aplicacao web
    - Request para pegar os inputs de dados das requisicoes
    - jsonify para transformar uma saida JSON em uma objeto de resposta para a aplicacao web
    - SQLAlchemy para acessar a base de dados
    - Marshmallow para converter tipos complexos de/para tipos nativos do Python
      Converter dado para um objeto de aplicacao
      Converter um objeto de aplicacao para um tipo simples
"""
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

# __init__ e config
# Cria a instancia da aplicacao web e seta o caminho para a URI do SQLite
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'api_crud.sqlite')

# Liga os modulos a aplicacao Flask
db = SQLAlchemy(app)
ma = Marshmallow(app)


# model
# Declaro o modelo do banco SQlite e define os campos e suas propriedades
class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(128))
    done = db.Column(db.Boolean)

    # Define o construtor
    def __init__(self, task, done):
        self.task = task
        self.done = done


# Define a estrutura da resposta para as requisicoes
class TaskSchema(ma.Schema):
    class Meta:
        fields = ('id', 'task', 'done')


task_schema = TaskSchema()
tasks_schema = TaskSchema(many=True)


# routes
# Rota principal, exibe 'Hello World!' quando carrega a página
@app.route('/')
def index():
    return 'Hello World!'


# Define a rota e qual o metodo HTTP (POST) para criar uma nova tarefa
@app.route('/api/tasks', methods=['POST'])
# Define a funcao a ser executada quando e realizada a requisicao
def create_task():
    if request.json['task'] == "":
        return 400
    else:
        # Recebe o nome da tarefa e se ela esta ou n�o realizada
        task = request.json['task']
        done = request.json['done']

        # Cria uma nova tarefa com os dados recebidos da requisicao
        new_task = Task(task, done)

        # Adiciona a nova tarefa no banco de dados e devolve como resposta essa tarefa no formato de um JSON
        db.session.add(new_task)
        db.session.commit()
        return jsonify(new_task), 201


# Define a rota e o metodo HTTP (GET) para ler todas as tarefas
@app.route('/api/tasks', methods=['GET'])
def get_tasks():
    # realiza uma query de todas as tarefas no banco e devolve em formato de um JSON
    all_tasks = Task.query.all()
    result = tasks_schema.dump(all_tasks)

    return jsonify(result.data), 200


# Define a rota(que agora recebe um parametro) e o metodo HTTP (GET) para ler UMA tarefa, baseado no id dela
@app.route('/api/tasks/<int:id>', methods=['GET'])
def get_one_task(id):
    # realiza uma query no banco utilizando o id da tarefa, e devolve em formato de um JSON
    task = Task.query.get(id)

    return task_schema.jsonify(task), 200


# Define a rota para o metodo PUT, que atualiza somente uma parte da tarefa, no caso o estado dela
@app.route('/api/tasks/<int:id>', methods=['PUT'])
def update_task(id):
    # Faz uma query pelo id passado como parametro e retorna para a variavel task
    task = Task.query.get(id)
    # Armazena na variavel done a resposta em json passada para a requisição
    done = request.json['done']

    #atualiza o estado da tarefa
    task.done = done

    #Comita no banco
    db.session.commit()
    return task_schema.jsonify(task), 200


# Define a rota para o metodo DELETE, que irá deletar uma tarefa pelo id passado como parametro
@app.route('/api/tasks/<int:id>', methods=['DELETE'])
def delete_task(id):
    # Faz uma query pelo id passado como parametro e retorna para a variavel task
    task = Task.query.get(id)
    # Deleta do banco a tarefa com o respectivo id
    db.session.delete(task)
    # Comita no banco
    db.session.commit()

    return task_schema.jsonify(task), 204


# Caso seja realizada alguma requisicao para uma rota nao existente
@app.errorhandler(404)
def nao_encontrado(error):
    return make_response(jsonify({error: 'Nao Encontrado'}), 404)


# Caso este arquivo esteja sendo executado sozinho, sem ser chamado por outro .py, roda esta aplicacao
if __name__ == "__main__":
    # Parametro debug caso esta setado com o valor True ira imprimir na pagina web erros Python.
    app.run(debug=True)
```

##

Vamos agora criar a nossa instância do banco SQLite, para isso vamos entrar no shell do Python:

-   cmd

```cmd
py -3
```

```python
>>> from api_crud import db
>>> db.create_all()
```

-   bash

```shell
$ python3
```

```python
>>> from api_crud import db
>>> db.create_all()
```

#

Agora podemos executar nossa aplicação

```
py -3
```

```python
>> api_crud.py
```
