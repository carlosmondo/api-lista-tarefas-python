import pytest
from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


DB_URL = "sqlite:///:memory:"


@pytest.fixture
def db_engine():
    engine = create_engine(DB_URL)
    engine.execute("pragma foreign_keys=on")
    yield engine
    engine.dispose()


@pytest.fixture
def db_sessionmaker(db_engine):
    alembic_cfg = Config("../migrations/alembic.ini")
    alembic_cfg.set_main_option("sqlalchemy.url", DB_URL)

    with db_engine.begin() as connection:
        alembic_cfg.attributes["connection"] = connection
        command.upgrade(alembic_cfg, "head")

    return sessionmaker(bind=db_engine)
