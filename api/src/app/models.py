from api.src.app import Base
from sqlalchemy import Boolean, Column, Integer, String


class Task(Base):
    __tablename__ = "task"

    id = Column(Integer, primary_key=True)
    task = Column(String(128), nullable=False)
    done = Column(Boolean, default=False)

    def __init__(self, task, done):
        self.task = task
        self.done = done

    @property
    def serialize(self):
        query = {"id": self.id, "task": self.task, "done": self.done}
        return query

    def __repr__(self):
        query = "Task<(id={}, task={}, done={})>".format(
            self.id, self.task, self.done
        )
        return query
