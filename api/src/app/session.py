from api.src.app import engine
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
