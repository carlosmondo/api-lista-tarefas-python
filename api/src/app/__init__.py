from config import Config
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

app = Flask(__name__)
app.config.from_object(Config)

Base = declarative_base()
engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)


Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
