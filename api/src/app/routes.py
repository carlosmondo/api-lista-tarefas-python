from http import HTTPStatus

from api.src.app import app
from api.src.app.models import Task
from api.src.app.session import Session
from flask import Response, jsonify, make_response, request


def commit_task(data):

    session = Session()

    task = Task(task=data.get("task"), done=data.get("done"))
    session.add(task)
    session.commit()

    data = {"id": task.id, "task": task.task, "done": task.done}
    session.close()
    return data


def validate_request(data):
    if data.get("task") == "" or data.get("done") is None:
        return False
    else:
        return True


@app.route("/")
def index():
    return "Hello World!"


@app.route("/api/tasks", methods=["POST"])
def create_task():

    data = request.get_json()

    if validate_request(data):
        commit_ok = commit_task(data)
        return make_response(jsonify(commit_ok), 201)

    else:
        return Response(status=HTTPStatus.NO_CONTENT)


@app.route("/api/tasks/", methods=["GET"])
@app.route("/api/tasks", methods=["GET"])
def get_tasks():
    session = Session()
    query = session.query(Task).all()

    if not query:
        session.close()
        return Response(status=HTTPStatus.NO_CONTENT)
    else:
        data = [
            {"id": task.id, "task": task.task, "done": task.done}
            for task in query  # noqa: E501
        ]

        session.close()
        return make_response(jsonify(data), 200)


@app.route("/api/tasks/<int:id>", methods=["PUT"])
def update_task(id):
    session = Session()
    data = request.get_json()
    task = session.query(Task).get(id)

    if not task:
        session.close()
        return Response(status=HTTPStatus.NOT_FOUND)
    else:

        task.done = data.get("done")
        session.commit()

        data = {"id": task.id, "task": task.task, "done": task.done}

        return make_response(jsonify(data), 200)


@app.route("/api/tasks/<int:id>", methods=["GET"])
def get_one_task(id):
    session = Session()
    task = session.query(Task).get(id)

    if not task:
        session.close()
        return Response(status=HTTPStatus.NOT_FOUND)
    else:

        data = {"id": task.id, "task": task.task, "done": task.done}
        session.close()
        return make_response(jsonify(data), 200)


@app.route("/api/tasks/<int:id>", methods=["DELETE"])
def delete_task(id):
    session = Session()
    task = session.query(Task).get(id)

    if not task:
        session.close()
        return Response(status=HTTPStatus.OK)
    else:
        data = {
            "id": task.id,
            "task": task.task,
            "done": task.done,
        }  # noqa: E501
        session.delete(task)
        session.commit()
        session.close()
        return make_response(jsonify(data), 200)
