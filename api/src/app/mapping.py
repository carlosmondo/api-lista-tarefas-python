from sqlalchemy import Boolean, Column, Integer, MetaData, String, Table
from sqlalchemy.orm import mapper

metadata = MetaData()

task = Table(
    "task",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("task", String(128), nullable=False),
    Column("done", Boolean, default=False),
)


class Task(object):
    def __init__(self, task_name, done):
        self.task = task_name
        self.done = done


mapper(Task, task)
