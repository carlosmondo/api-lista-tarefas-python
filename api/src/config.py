import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "voce-nunca-ira-saber"
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(
        basedir, "../db/api.db"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JSON_SORT_KEYS = False
